package mattermost

import (
	"pheromasa/config"
	"pheromasa/loggers"

	"github.com/mattermost/mattermost-server/v5/model"
)

func PostMessage(client *model.Client4, message, rootId string) (*model.Post, error) {
	logger := loggers.GetLogger()

	post := model.Post{
		Message:   message,
		ChannelId: config.GetChannelID(),
		RootId:    rootId,
	}
	p, resp := client.CreatePost(&post)
	if resp.Error != nil {
		logger.Error(resp)
		return nil, resp.Error
	}
	return p, nil
}
