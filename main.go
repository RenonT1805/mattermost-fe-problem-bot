package main

import (
	"os"
	"pheromasa/config"
	"pheromasa/loggers"

	"github.com/kardianos/service"
)

var logger service.Logger

func main() {
	svcConfig := &service.Config{
		Name:        config.AppName,
		DisplayName: config.AppName,
		Description: config.AppName,
	}

	program := &app{}
	program.exit = make(chan struct{})
	s, err := service.New(program, svcConfig)
	if err != nil {
		panic(err)
	}

	loggers.Initialize(s)
	logger = loggers.GetLogger()

	if len(os.Args) > 1 {
		err = service.Control(s, os.Args[1])
		if err != nil {
			logger.Info("Failed (%s) : %s\n", os.Args[1], err)
			return
		}
		logger.Info("Succeeded (%s)\n", os.Args[1])
	} else {
		s.Run()
	}
}
