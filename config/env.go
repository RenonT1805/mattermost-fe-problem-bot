package config

import (
	"os"
	"strings"
)

const (
	AppName = "pheromasa"
)

// GetBotToken : MattermostのBotTokenを取得する
func GetBotToken() string {
	return os.Getenv(strings.ToUpper(AppName) + "_TOKEN")
}

// GetChannelID : 投稿するチャンネルIDをを取得する
func GetChannelID() string {
	return os.Getenv(strings.ToUpper(AppName) + "_CHANNEL_ID")
}
