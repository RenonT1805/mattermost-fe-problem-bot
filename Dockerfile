FROM golang:latest
ENV GOPATH=/go/

CMD cd /go/src/pheromasa && go build && ./pheromasa