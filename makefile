run:
	docker-compose build
	docker-compose up

deploy:
	docker-compose up -d

down:
	docker-compose down