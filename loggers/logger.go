package loggers

import (
	"log"

	"github.com/kardianos/service"
)

var logger service.Logger

// Initialize : loggerを初期化する
func Initialize(s service.Service) error {
	var err error
	errs := make(chan error, 5)
	logger, err = s.Logger(errs)
	if err != nil {
		return err
	}

	go func() {
		for {
			err := <-errs
			if err != nil {
				log.Print(err)
			}
		}
	}()
	return nil
}

// GetLogger : loggerを取得する
func GetLogger() service.Logger {
	return logger
}
