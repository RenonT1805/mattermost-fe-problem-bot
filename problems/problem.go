package problems

import (
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"

	"golang.org/x/text/encoding/japanese"
	"golang.org/x/text/transform"
)

const origin = `https://www.fe-siken.com`

type Problem struct {
	Sentence      string
	Choices       [4]string
	CorrectChoice int
	ImageURL      string
	ProblemURL    string
}

func httpRequest(method, path string) (string, error) {
	request, err := http.NewRequest(method, path, nil)
	if err != nil {
		return "", err
	}

	client := http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return "", err
	}

	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func getProblemURL() string {
	str, err := httpRequest("GET", origin+"/")
	if err != nil {
		panic(err)
	}
	str = str[:strings.Index(str, `" class="ansbtn">`)]
	str = str[strings.LastIndex(str, `<a href="`)+len(`<a href="`):]
	return origin + str[1:]
}

func GetProblem() Problem {
	var result Problem
	result.ProblemURL = getProblemURL()
	str, err := httpRequest("GET", result.ProblemURL)
	if err != nil {
		panic(err)
	}
	decoder := japanese.ShiftJIS.NewDecoder()
	reader := transform.NewReader(strings.NewReader(string(str)), decoder)
	bytes, err := ioutil.ReadAll(reader)

	source := string(bytes)
	str = source[strings.Index(source, `class="qno"`):]
	str = str[strings.Index(str, `</h3>`):]
	str = str[strings.Index(str, `<div>`)+len(`<div>`):]
	str = str[:strings.Index(str, `</div>`)]
	result.Sentence = str

	if i := strings.Index(str, `img src="`); i != -1 {
		result.Sentence = str[:strings.Index(result.Sentence, `<div`)]

		str = str[i+len(`img src="`):]
		str = str[:strings.Index(str, "\"")]

		result.ImageURL = result.ProblemURL[:strings.LastIndex(result.ProblemURL, "/")]
		result.ImageURL += "/" + str
	}

	if regexp.MustCompile("<.+>").MatchString(result.Sentence) {
		return GetProblem()
	}

	str = source[strings.Index(source, `class="ansbg"`):]
	str = str[strings.Index(str, `<ul`):strings.Index(str, "</ul>")]

	choiceNames := []string{"a", "i", "u", "e"}
	for i, name := range choiceNames {
		if strings.Index(str, `<div id="select_`+name+`">`) == -1 {
			return GetProblem()
		}
		choiceSentence := str[strings.Index(str, `<div id="select_`+name+`">`):]
		choiceSentence = choiceSentence[len(`<div id="select_`+name+`">`):strings.Index(choiceSentence, `</div>`)]
		result.Choices[i] = choiceSentence
	}

	str = source[strings.Index(source, `<span id="answerChar">`):]
	str = str[len(`<span id="answerChar">`):strings.Index(str, "</span>")]

	switch str {
	case "ア":
		result.CorrectChoice = 1
	case "イ":
		result.CorrectChoice = 2
	case "ウ":
		result.CorrectChoice = 3
	case "エ":
		result.CorrectChoice = 4
	default:
		return GetProblem()
	}
	return result
}
