package main

import (
	"pheromasa/config"
	"pheromasa/loggers"
	"pheromasa/mattermost"
	"pheromasa/problems"
	"strconv"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/kardianos/service"
	"github.com/mattermost/mattermost-server/v5/model"
)

type app struct {
	exit    chan struct{}
	discord *discordgo.Session
}

func (a *app) run() {
	logger = loggers.GetLogger()
	logger.Info("BotToken : " + config.GetBotToken())

	client := model.NewAPIv4Client("https://chatot.rx-apps.net")
	client.AuthToken = config.GetBotToken()
	client.AuthType = "Bearer"

	const (
		sleepTime     = 5
		timeupSeconds = 1800
	)

	for {
		jpTime := time.Now().Add(time.Hour * 9)
		if !((jpTime.Hour() == 15 && jpTime.Minute() == 0) || (jpTime.Hour() == 10 && jpTime.Minute() == 0)) {
			time.Sleep(time.Duration(sleepTime) * time.Second)
			continue
		}

		problem := problems.GetProblem()
		logger.Info(problem.ProblemURL)

		var sentence string
		sentence += "皆様、お疲れ様です。基本情報技術者試験の問題を出題します。\n"
		sentence += "ぜひ奮ってご参加ください。\n"
		sentence += "\n"
		sentence += problem.Sentence + "\n"
		if problem.ImageURL != "" {
			sentence += problem.ImageURL + "\n"
		}
		for i, choice := range problem.Choices {
			sentence += strconv.Itoa(i+1) + ". " + choice + "\n"
		}
		sentence += "\n"
		sentence += "正解は３０分後に投稿されます。\n"
		sentence += "回答はこのメッセージに「1」のように 1 ~ 4 までの半角数字でリプライしてください。"

		problemPost, _ := mattermost.PostMessage(client, sentence, "")

		count := 0
		repliedPosts := map[string]struct{}{}
		answers := map[string]int{}

		for count < timeupSeconds/sleepTime {
			postList, resp := client.GetPostThread(problemPost.Id, "", false)
			if resp.Error != nil {
				logger.Error(resp.Error)
				continue
			}
			for _, post := range postList.Posts {
				if post.Id == problemPost.Id {
					continue
				}
				if _, exist := repliedPosts[post.Id]; exist {
					continue
				}
				repliedPosts[post.Id] = struct{}{}
				if v, err := strconv.Atoi(post.Message); err == nil {
					if 0 < v || v < 5 {
						answers[post.UserId] = v
						continue
					}
				}
				user, resp := client.GetUser(post.UserId, "")
				if resp.Error != nil {
					logger.Error(resp.Error)
					continue
				}
				post, err := mattermost.PostMessage(client, "@"+user.Username+" 回答は 1 ~ 4 までの半角数字でリプライしてください。", problemPost.Id)
				if err == nil {
					repliedPosts[post.Id] = struct{}{}
				}
			}
			count++
			time.Sleep(time.Duration(sleepTime) * time.Second)
		}

		sentence = "先程の問題の正解は " + strconv.Itoa(problem.CorrectChoice) + " です。\n"
		sentence += "参照URL：" + problem.ProblemURL + "\n"

		if len(answers) == 0 {
			sentence += "回答者はいませんでした…\n次回は是非、ご参加ください。"
		} else {
			var correctUserSentence string
			for userId, answer := range answers {
				user, resp := client.GetUser(userId, "")
				if resp.Error != nil {
					logger.Error(resp.Error)
					continue
				}
				if answer == problem.CorrectChoice {
					correctUserSentence += "@" + user.Username + " さん\n"
				}
			}
			if correctUserSentence == "" {
				sentence += "正解者はいませんでした…\n"
			} else {
				sentence += "正解者は次の方です。\n"
				sentence += correctUserSentence
			}
			sentence += "ご参加、ありがとうございました。"
		}

		_, err := mattermost.PostMessage(client, sentence, "")
		if err != nil {
			logger.Error(err)
		}
	}
}

func (a *app) Start(s service.Service) error {
	go a.run()
	return nil
}

func (a *app) Stop(s service.Service) error {
	close(a.exit)
	return nil
}
