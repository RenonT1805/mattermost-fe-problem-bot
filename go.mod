module pheromasa

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/kardianos/service v1.2.0
	github.com/mattermost/mattermost-server/v5 v5.35.1
	golang.org/x/text v0.3.5
)
